angular.module('bengaldealsApp',['ngRoute','ngCookies']);

angular.module('bengaldealsApp').config(['$routeProvider','$locationProvider',
    function($routeProvider,$locationProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'app/views/home.html',
                controller: 'homeController'
            }).
            when('/login', {
                templateUrl: 'app/views/login.html',
                controller: 'loginController'
            }).
            when('/dashboard', {
                templateUrl: 'app/views/dashboard.html',
                controller: 'dashboardController'
            }).
            when('/logout', {
                templateUrl: 'app/views/logout.html',
                controller: 'logoutController'
            }).
            when('/offer/:offer_id', {
                templateUrl: 'app/views/offer.html',
                controller: 'offerViewController'
            }).
            when('/add-offer/', {
                templateUrl: 'app/views/add_offer.html',
                controller: 'addOfferController'
            }).
            when('/shop/:shop_id', {
                templateUrl: 'app/views/shop.html',
                controller: 'shopViewController'
            }).
            when('/user', {
                templateUrl: 'app/views/user.html',
                controller: 'userViewController'
            }).
            when('/shops', {
                templateUrl: 'app/views/shops.html',
                controller: 'shopViewController'
            }).
            when('/add-shop', {
                templateUrl: 'app/views/add_shop.html',
                controller: 'addShopController'
            }).
            otherwise({
                redirectTo: '/'
            });
        // $locationProvider.html5Mode(true);
    }]);

angular.module('bengaldealsApp').controller( 'mainController', function( $scope,$cookieStore, AuthService ) {
    $scope.$watch( AuthService.isLoggedIn, function ( isLoggedIn ) {
        $scope.isLoggedIn = isLoggedIn;
        $scope.currentUser = $cookieStore.get('user');//AuthService.currentUser();
    });

    $scope.$on('loginEvent', function (event, data) {
        if($cookieStore.get('isLoggedIn')==undefined || $cookieStore.get('user')==undefined ||$cookieStore.get('isLoggedIn')==false){
            //$scope.$emit('logoutEvent',data);
            console.log('here');
            $scope.loginButton="LOGIN / REGISTER";
            $scope.loginButton.show=true;
            $scope.loginButtonUrl='login';
        }else{
            $scope.loginButton="DASHBOARD";
            $scope.loginButton.show=true;
            $scope.currentUser = $cookieStore.get('user');
            $scope.userId=$cookieStore.get('user_id');
            $scope.loginButtonUrl='dashboard';
        }

    });

    $scope.$on('logoutEvent', function (event, data) {
        $scope.loginButton="LOGIN / REGISTER";
        $scope.loginButtonUrl='login';
        $scope.currentUser=undefined;
        $cookieStore.remove('user_id');
        $cookieStore.remove('user');
        $cookieStore.remove('isLoggedIn');
    });
});

angular.module('bengaldealsApp').factory( 'AuthService', function($http,$cookieStore,$location) {
    var currentUser;

    return {
        login: function(email,password) {
            var req = {
                method: 'POST',
                url: '/api/login',
                headers: {
                    'Content-Type': "application/x-www-form-urlencoded",
                    'Access-Control-Allow-Origin': 'POST, GET, OPTIONS, PUT',
                    'Authorization': "Bearer LUotxme72w9ROYAX0gcP"
                },
                data: "email="+email+"&password="+password+"&grant_type=password"
            };
            $http(req).success(function(response){
                console.log(response);
                if(response.success){
                    $cookieStore.put('user',response.user.name);
                    console.log(response.user.name);
                    $cookieStore.put('isLoggedIn',true);
                    currentUser = response.user;
                    $location.path('/');
                    $location.replace();
                }
            }).error(function(response){
                console.log(response);
            })
        },
        logout: function() {
            $http.get("/api/logout/")
                .then(function(response) {
                    if(response.success){
                        $cookieStore.put('user',"");
                        $cookieStore.put('isLoggedIn',false);
                    }
                });
        },
        register: function(name,email,password) {
            var req = {
                method: 'POST',
                url: '/api/register',
                headers: {
                    'Content-Type': "application/x-www-form-urlencoded",
                    'Access-Control-Allow-Origin': 'POST, GET, OPTIONS, PUT',
                },
                data: "name="+name+"&email="+email+"&password="+password//+"&grant_type=password"
            };
            $http(req).success(function(response){
                console.log(response);

            }).error(function(response){
                console.log(response);
            })
        },
        isLoggedIn: function() {
            $cookieStore.get('isLoggedIn');
        },
        currentUser: function() {
            if(currentUser){
                return currentUser.name;
            }
        }
    };
});


angular.module('bengaldealsApp').controller('homeController', function($scope,$http) {
    $scope.$emit('loginEvent',[]);
    $http.get("/api/offer")
        .then(function(response) {$scope.offers = response.data.offers;});
});

angular.module('bengaldealsApp').controller('dashboardController', function($scope,$http) {
    $scope.$emit('loginEvent',[]);
    //$http.get("/api/offer")
    //    .then(function(response) {$scope.offers = response.data.offers;});
});



angular.module('bengaldealsApp').controller('loginController',function($scope,$cookieStore,$location,AuthService){
    $scope.message= 'ok';
        if($cookieStore.get('isLoggedIn')==undefined || $cookieStore.get('user')==undefined ||$cookieStore.get('isLoggedIn')==false) {
            $scope.loginClicked = function() {
                var email = $scope.login_email;
                var password = $scope.login_password;

                AuthService.login(email, password);
                $scope.$emit('loginEvent', []);
            }
        }else{
            $location.path('/');
            $location.replace();
        }

    console.log("hello from the other side");
});


angular.module('bengaldealsApp').controller( 'logoutController', function( $scope,$http,$cookieStore,$location, AuthService ) {
    $http.get("/api/logout/")
        .then(function(response) {
            $cookieStore.put('user',undefined);
            $cookieStore.put('isLoggedIn',false);
        });
    console.log("ok logged out")
    $scope.$emit('logoutEvent',[]);
    $location.path('/');
    $location.replace();
});


angular.module('bengaldealsApp').controller('offerViewController', function($scope,$routeParams,$http) {

    $http.get("/api/offer/"+$routeParams.offer_id)
        .then(function(response) {
            $scope.offer = response.data.offer;
        });
});

angular.module('bengaldealsApp').controller('shopViewController', function($scope,$routeParams,$http) {

    $http.get("/api/shop/"+$routeParams.shop_id)
        .then(function(response) {
            $scope.shop = response.data.shop;
        });
});

angular.module('bengaldealsApp').controller('userViewController', function($scope,$routeParams,$location,$http) {

    $http.get("/api/user/")
        .then(function(response) {
            if(response.data.user){
                $scope.user = response.data.user;
            }else{
                $location.path('/');
                $location.replace();
            }
        });
});

angular.module('bengaldealsApp').controller('shopController', function($scope,$routeParams,$http) {

    $http.get("/api/shop/")
        .then(function(response) {
            $scope.shops = response.data.shops;
        });
});

angular.module('bengaldealsApp').controller('addShopController', function($scope,$routeParams,$http) {
    $scope.addShop = function(){
        var req = {
            method: 'POST',
            url: '/api/shop',
            headers: {
                'Content-Type': "application/x-www-form-urlencoded",
                'Access-Control-Allow-Origin': 'POST, GET, OPTIONS, PUT',
            },
            data: "name="+$scope.name+"&description="+$scope.description+"&location="+$scope.location//+"&grant_type=password"
        };
        $http(req).success(function(response){
            console.log(response);
            $scope.message = response.message;
        }).error(function(response){
            console.log(response);
        })
    }
});


angular.module('bengaldealsApp').controller('addOfferController', function($scope,$routeParams,$http) {
    $scope.addOffer = function(){
        var req = {
            method: 'POST',
            url: '/api/offer',
            headers: {
                'Content-Type': "application/x-www-form-urlencoded",
                'Access-Control-Allow-Origin': 'POST, GET, OPTIONS, PUT',
            },
            data: "name="+$scope.name+"&description="+$scope.description+"&shop_id="+$scope.shop_id//+"&grant_type=password"
        };
        $http(req).success(function(response){
            console.log(response);
            $scope.message = response.message;
        }).error(function(response){
            console.log(response);
        })
    }
});


angular.module('bengaldealsApp').controller('shopController', function($scope,$routeParams,$http) {

    $http.get("/api/shop/")
        .then(function(response) {
            $scope.shops = response.data.shops;
        });
});


angular.module('bengaldealsApp').factory( 'ShopService', function($cookieStore,$http) {

    return {
        addShop: function(email,description,location) {
            var req = {
                method: 'POST',
                url: '/api/shop',
                headers: {
                    'Content-Type': "application/x-www-form-urlencoded",
                    'Access-Control-Allow-Origin': 'POST, GET, OPTIONS, PUT',
                },
                data: "name="+name+"&description="+description+"&location="+location
            };
            $http(req).success(function(response){
                console.log(response);

            }).error(function(response){
                console.log(response);
            })
        },
        deleteShop: function(shop_id) {
            var req = {
                method: 'POST',
                url: '/api/shop'+shop_id,
                headers: {
                    'Content-Type': "application/x-www-form-urlencoded",
                },
                data: "shop_id="+id+"&_method=delete"
            };
            $http(req).success(function(response){
                console.log("delete success");
            }).error(function(response){
                console.log("delete failed");
            })
        },
        updateShop: function(name,description,location,shop_id) {

        },
    };
});