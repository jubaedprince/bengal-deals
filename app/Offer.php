<?php

namespace App;

use Ghanem\Rating\Contracts\Rating;
use Ghanem\Rating\Traits\Ratingable as RatingTrait;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Offer extends Model
{
    use RatingTrait;

    protected $table = 'offers';

    protected $fillable = ['name', 'description', 'shop_id'];

    protected $appends = ['rating'];

    protected $hidden = [];

    public function shop(){

        return $this->belongsTo('App\Shop');
    }

    public function rate($value){
        $user = Auth::User();

        return $rating = $this->rating([
            'rating' => $value
        ], $user);
    }

    public function updateRate($id, $value){
        return $rating = $this->updateRating($id, [
                'rating' => $value
            ]);
    }

    public function getRatingAttribute(){
        return $this->averageRating();
    }

}
