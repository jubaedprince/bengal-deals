<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Shop;
use Illuminate\Support\Facades\Auth;
use Validator;

class ShopController extends Controller
{

    public function index()
    {
        $shops = Shop::orderBy('created_at', 'DESC')->get();
        return response()->json([
            'success'=>true,
            'message'=>"All shops are found",
            'shops'=> $shops,
        ]);
    }


    public function store(Request $request){
        //validate data
        $validator = Validator::make($request->all(), array(
            'name'          => 'required|max:255',
            'location'      => '',
            'description'   => ''
        ));

        if ($validator->fails()) {
            return response()->json([
                'success'=>false,
                'message'=>"Validation error",
                'error'=> $validator->messages()
            ]);

        } else {

            $shop =
                Shop::create([
                'name'          => $request['name'],
                'location'      => $request['location'],
                'description'   => $request['description'],
                'user_id'       => Auth::user()->id,
            ]);

            return response()->json([
                'success'   =>  true,
                'message'   => "Shop created Successfully",
                'shop'      => $shop,
            ]);
        }
    }


    public function show($id)
    {
        $shop = Shop::find($id);

        if($shop == null){
            return response()->json([
                'success'=>false,
                'message'=>"No shop found",
            ]);
        }

        return response()->json([
            'success'=>true,
            'message'=>"Shop found",
            'shop'=> $shop,
        ]);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), array(
            'name'          => 'required|max:255',
            'location'      => '',
            'description'   => ''
        ));

        if ($validator->fails()) {
            return response()->json([
                'success'=>false,
                'message'=>"Validation error",
                'error'=> $validator->messages()
            ]);
        }

        else{
            $shop = Shop::find($id);

            if($shop == null){
                return response()->json([
                    'success'=>false,
                    'message'=>"No shop found",
                ]);
            }

            if($shop->user_id !=  Auth::User()->id){
                return response()->json([
                    'success'=>false,
                    'message'=>"You are not the owner of this shop",
                ]);
            }

            $shop->name = $request->input('name');
            $shop->description = $request->input('description');
            $shop->location = $request->input('location');
            $shop->save();

            return response()->json([
                'success'=>true,
                'message'=>"Shop updated",
                'shop'=> $shop,
            ]);
        }
    }

    public function destroy($id)
    {
        $shop = Shop::find($id);

        if($shop == null){
            return response()->json([
                'success'=>false,
                'message'=>"No shop found",
            ]);
        }

        if($shop->user_id !=  Auth::User()->id){
            return response()->json([
                'success'=>false,
                'message'=>"You are not the owner of this shop",
            ]);
        }

        $result = Shop::destroy($id);

        if($result==0){
            return response()->json([
                'success'=>false,
                'message'=>"Could not delete",
            ]);
        }
        return response()->json([
            'success'=>true,
            'message'=>"Shop removed",
        ]);
    }
}
