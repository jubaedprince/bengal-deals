<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Offer;
use Auth;
use Validator;
use Ghanem\Rating\Models\Rating;
class OfferController extends Controller
{

    public function index()
    {
        $offers = Offer::orderBy('created_at', 'DESC')->with('shop')->get();
        return response()->json([
            'success'=>true,
            'message'=>"All offers are found",
            'offers'=> $offers,
        ]);
    }


    public function store(Request $request){
        //validate data
        $validator = Validator::make($request->all(), array(
            'name'          => 'required|max:255',
            'description'   => ''
        ));

        if ($validator->fails()) {
            return response()->json([
                'success'=>false,
                'message'=>"Validation error",
                'error'=> $validator->messages()
            ]);

        } else {

            $offer =
                Offer::create([
                    'name'          => $request['name'],
                    'description'   => $request['description'],
                    'shop_id'       => $request['shop_id'],
                ]);

            return response()->json([
                'success'   =>  true,
                'message'   => "Offer created Successfully",
                'shop'      => $offer,
            ]);
        }
    }


    public function show($id)
    {
        $offer = Offer::with('shop')->find($id);

        if($offer == null){
            return response()->json([
                'success'=>false,
                'message'=>"No offer found",
            ]);
        }

        return response()->json([
            'success'=>true,
            'message'=>"Offer found",
            'offer'=> $offer,
        ]);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), array(
            'name'          => 'required|max:255',
            'description'   => ''
        ));

        if ($validator->fails()) {
            return response()->json([
                'success'=>false,
                'message'=>"Validation error",
                'error'=> $validator->messages()
            ]);
        }

        else{
            $offer = Offer::find($id);

            if($offer == null){
                return response()->json([
                    'success'=>false,
                    'message'=>"No offer found",
                ]);
            }

            if($offer->shop->user->id!=  Auth::User()->id){
                return response()->json([
                    'success'=>false,
                    'message'=>"You are not the owner of this shop",
                ]);
            }

            if ($request->input('name') != null){
                $offer->name = $request->input('name');
            }

            if ($request->input('description') != null){
                $offer->description = $request->input('description');
            }

            if ($request->input('location') != null){
                $offer->location = $request->input('location');
            }

            $offer->save();

            return response()->json([
                'success'=>true,
                'message'=>"Shop updated",
                'offer'=> $offer,
            ]);
        }
    }

    public function destroy($id)
    {
        $offer = Offer::find($id);

        if($offer == null){
            return response()->json([
                'success'=>false,
                'message'=>"No offer found",
            ]);
        }

        if($offer->shop->user->id !=  Auth::User()->id){
            return response()->json([
                'success'=>false,
                'message'=>"You are not the owner of this offer",
            ]);
        }

        $result = Offer::destroy($id);

        if($result==0){
            return response()->json([
                'success'=>false,
                'message'=>"Could not delete",
            ]);
        }
        return response()->json([
            'success'=>true,
            'message'=>"Shop removed",
        ]);
    }

    public function rate(Request $request, $id){
        $validator = Validator::make($request->all(), array(
            'rate'          => 'required|integer|min:0|max:5'
        ));

        if ($validator->fails()) {
            return response()->json([
                'success'=>false,
                'message'=>"Validation error",
                'error'=> $validator->messages()
            ]);
        }

        else{
            $offer = Offer::find($id);

            if($offer == null){
                return response()->json([
                    'success'=>false,
                    'message'=>"No offer found",
                ]);
            }
            //checks if rating was already done by current user
            $rating = Rating::where('author_id', Auth::User()->id)->where('ratingable_id', $id)->first();
            if ($rating){//if so then updates the rating
                $offer->updateRate($rating->id, $request->input('rate'));
            }else{//else creates a new rating
                $offer->rate($request->input('rate'));
            }

            return response()->json([
                'success'=>true,
                'message'=>"Offer rated",
                'offer'=> $offer,
            ]);
        }
    }
}
