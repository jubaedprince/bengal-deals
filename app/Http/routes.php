<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix'=>'api'], function(){

    //Logs in a user.
    Route:: post('login', 'UserController@login');

    //Registers a user
    Route:: post('register', 'UserController@register');

    //Logout a user.
    Route:: get('logout', 'UserController@logout');

    //Shop related routes
    Route::resource('shop', 'ShopController',
        ['only' => ['index', 'show']]);

    //Offer related routes
    Route::resource('offer', 'OfferController',
        ['only' => ['index', 'show']]);





    //Authenticated Routes

    //     __________
    //    |          |
    //    |          |
    //    |          |
    //    ============
    //    ============
    //    ||        ||
    //    ||   ___  ||
    //    ||   | |  ||
    //    ||   |_|  ||
    //    ||        ||
    //    =============

    Route::group(['middleware'=>'auth'],function(){

        //Get logged in user info.
        Route:: get('user', 'UserController@user');

        //Shop related routes
        Route::resource('shop', 'ShopController',
            ['only' => ['store', 'update', 'destroy']]);

        //Offer related routes
        Route::resource('offer', 'OfferController',
            ['only' => ['store', 'update', 'destroy']]);

        //Rate a offer
        Route::post('offer/{offer}/rate', 'OfferController@rate');

    });


    //===========//=============//=================//====================//========================//=============================//
    //Documentation
    Route::get('/', function () {
        return response()->json([
            'success'   =>  true,
            'message'   =>  "You are in the API of BengalDeals.",
            'API'       =>  [

                'login' => [
                    'url'       => '/login',
                    'method'    => 'POST',
                    'params'    => 'email, password',
                    'response'  => 'success: true/false',
                    'task'      => 'Logs in a user'],

                'register' => [
                    'url'       => '/register',
                    'method'    => 'POST',
                    'params'    => 'name, email, password',
                    'response'  => 'success: true/false',
                    'task'      => 'Registers a user or sends validation message.'],

                'logout' => [
                    'url'       => '/logout',
                    'method'    => 'GET',
                    'response'  => 'success: true',
                    'task'      => 'Logs out current user'],

                'current user'  => [
                    'url'       => '/user',
                    'method'    => 'GET',
                    'response'  => 'success: true [authentication required]',
                    'task'      => 'Gets current logged in user'],

                'all shop'  => [
                    'url'       => '/shop',
                    'method'    => 'GET',
                    'response'  => 'success: true',
                    'task'      => 'Gets all shops'],

                'shop by id'  => [
                    'url'       => '/shop/{id}',
                    'method'    => 'GET',
                    'response'  => 'success: true, false',
                    'task'      => 'Gets one shop by id'],

                'delete shop'  => [
                    'url'       => '/shop/{id}',
                    'method'    => 'DELETE',
                    'response'  => 'success: true, false [authentication required]',
                    'task'      => 'Removes one shop by id'],

                'create shop'  => [
                    'url'       => '/shop',
                    'method'    => 'POST',
                    'response'  => 'success: true, false [authentication required]',
                    'task'      => 'Creates one shop'],

                'update shop'  => [
                    'url'       => '/shop/{id}',
                    'method'    => 'PUT/PATCH',
                    'response'  => 'success: true, false [authentication required]',
                    'task'      => 'Update one shop'],


                'all offers'  => [
                    'url'       => '/offer',
                    'method'    => 'GET',
                    'response'  => 'success: true',
                    'task'      => 'Gets all offers'],

                'offer by id'  => [
                    'url'       => '/offer/{id}',
                    'method'    => 'GET',
                    'response'  => 'success: true, false',
                    'task'      => 'Gets one offer by id'],

                'delete offer'  => [
                    'url'       => '/offer/{id}',
                    'method'    => 'DELETE',
                    'response'  => 'success: true, false [authentication required]',
                    'task'      => 'Removes one offer by id'],

                'create offer'  => [
                    'url'       => '/offer',
                    'method'    => 'POST',
                    'response'  => 'success: true, false [authentication required]',
                    'task'      => 'Creates one offer'],

                'update offer'  => [
                    'url'       => '/offer/{id}',
                    'method'    => 'PUT/PATCH',
                    'response'  => 'success: true, false [authentication required]',
                    'task'      => 'Update one offer'],

                'rate offer'  => [
                    'url'       => '/offer/{id}/rate',
                    'method'    => 'POST',
                    'response'  => 'success: true, false [authentication required]',
                    'task'      => 'Rate a offer'],
            ]
        ]);

    });
    //===========//=============//=================//====================//========================//=============================//
});

