<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table = 'shops';

    protected $fillable = ['name', 'location', 'description', 'user_id'];

    protected $hidden = [];

    public function user(){

        return $this->belongsTo('App\User');
    }

    public function offers(){

        return $this->hasMany('App\Offer');
    }
}
