<?php

use Illuminate\Database\Seeder;
use App\Offer;

class OfferTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Offer::truncate();

        for ($i=0; $i < 10; $i++) {
            Offer::create([
                'name'=> $faker->company,
                'description' => $faker->text(200),
                'shop_id' => $faker->numberBetween(1,9)
            ]);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
