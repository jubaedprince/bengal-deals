<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        User::truncate();

        User::create([
            'name'=> 'Gulshan Jubaed Prince',
            'email' => 'jubaedprince@hotmail.com',
            'password' => Hash::make('123456789')
        ]);

        for ($i=0; $i < 10; $i++) {
            User::create([
                'name'=> $faker->name,
                'email' => $faker->email,
                'password' => Hash::make('123456789')
            ]);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
