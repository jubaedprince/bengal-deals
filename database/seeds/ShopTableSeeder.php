<?php

use Illuminate\Database\Seeder;
use App\Shop;

class ShopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Shop::truncate();

        for ($i=0; $i < 10; $i++) {
            Shop::create([
                'name'=> $faker->company,
                'location' => $faker->address,
                'description' => $faker->text(200),
                'user_id' => $faker->numberBetween(1,9)
            ]);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
