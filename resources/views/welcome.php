<!DOCTYPE html>
<html lang="en" ng-app="bengaldealsApp" ng-controller="mainController">

<head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <base href="/">
    <script src='https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-beta.2/angular.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.0-beta.2/angular-route.min.js'></script>
    <script src='app/js/angular-cookies.js'></script>

    <link rel="stylesheet" href="res/css/style.css" type="text/css"/>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:400,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <title>Bengal Deals</title>
</head>

<body class="container">

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/#/">
                <p>BENGAL DEALS</p>
            </a>
        </div>
        <div class="navbar-header">
            <a class="navbar-brand" href="/#/shops">
                <p>All SHOPS</p>
            </a>
        </div>
        <div ng-if="currentUser" class="navbar-header navbar-right">
            <a class="navbar-brand" href="/#/user">
                <p>{{currentUser}}</p>
            </a>
        </div>
        <div class="navbar-header navbar-right">
            <a class="navbar-brand" href="/#/{{loginButtonUrl}}">
                <p>{{loginButton}}</p>
            </a>
        </div>
    </div>
</nav>

<div style="padding-top: 70px" ng-view>

</div>
<script src='app/app.js'></script>
<script src='app/controllers/login_controller.js'></script>
</body>
</html>
